libtext-sass-perl (1.0.4-3) UNRELEASED; urgency=medium

  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.
  * Update standards version to 4.5.0, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 12 Mar 2020 19:43:38 +0000

libtext-sass-perl (1.0.4-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update a couple of URLs to use HTTPS..
  * Remove libreadonly-xs-perl from Recommends.
    Not needed since libreadonly-perl 1.500.0, and about to be removed from
    the archive.
  * Update lintian override (changed tag name).

  [ Jonas Smedegaard ]
  * Simplify rules
  * Stop build-depend on dh-buildinfo or cdbs.
  * Declare compliance with Debian Policy 4.4.0.
  * Set Rules-Requires-Root: no.
  * Wrap and sort control file.
  * Update copyright info: Extend coverage of packaging.
  * Fix shebang.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 15 Jul 2019 15:13:52 -0300

libtext-sass-perl (1.0.4-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Support single-line comments.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Bump to file format 4.
    + Watch only MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
  * Drop CDBS get-orig-source target: Use gbp import-orig --uscan.
  * Update copyright info: Extend coverage of Debian packaging.
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize Vcs-Git field: Use https protocol.
  * Modernize CDBS use: Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 25 Dec 2016 23:07:34 +0100

libtext-sass-perl (1.0.3-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 23 Nov 2015 13:42:00 +0100

libtext-sass-perl (0.98.0-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Jonas Smedegaard ]
  * Update CDBS hints for upstream tarball.
  * Update copyright info:
    + Add alternate git source URL.
    + Extend copyright of packaging to cover current year.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Sort and newline-delimit dependencies.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 26 Oct 2015 16:46:57 +0100

libtext-sass-perl (0.97-6) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Mark package as autopkgtest-able
  * Declare compliance with Debian Policy 3.9.6
  * Remove 'perl' alternative from libmodule-build-perl build dependency
  * skip t/00-distribution in smoke tests
  * Mention module name in the long description

 -- Damyan Ivanov <dmn@debian.org>  Thu, 11 Jun 2015 20:45:31 +0000

libtext-sass-perl (0.97-5) unstable; urgency=medium

  * Fix use canonical Vcs-Git URL.
  * Update watch file to use www.cpan.org and metacpan.org URLs (not
    search.cpan.org URL), and drop superfluous version mangling.
  * Update copyright info:
    + Extend coverage of packaging to include current year.
  * Fix favor build-dependency on recent perl over recent
    libmodule-build-perl, and also build-depend unversioned on
    libmodule-build-perl (see bug#752989).
    Thanks to Niko Tyni and Gregor Herrmann.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Jul 2014 17:20:24 +0200

libtext-sass-perl (0.97-4) unstable; urgency=medium

  * Fix depend (not only build-depend) on libconvert-color-perl.
  * Bump standards-version to 3.9.5.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 24 Apr 2014 10:29:50 +0200

libtext-sass-perl (0.97-3) unstable; urgency=low

  * Fix switch order of build-dependency to favor libmodule-build-perl
    over newer-than-unstable perl.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 01 Jul 2013 16:05:30 +0200

libtext-sass-perl (0.97-2) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Update copyright file:
    + Fix use pseudo-license and pseudo-comment sections to obey silly
      restrictions of copyright format 1.0.
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Bump standards-version to 3.9.4.
  * Bump packaging license to GPL-3+, and extend copyrigt coverage for
    myself to include recent years.
  * Update package relations:
    + Fix versioned build-dependency on libmodule-build-perl, and favor
      (very) recent perl.
    + Relax to build-depend unversioned on libconvert-color-perl: Needed
      version satisfied even in oldstable.

  [ Salvatore Bonaccorso ]
  * Use canonical hostname (anonscm.debian.org) in Vcs-Git URI.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 01 Jul 2013 15:41:07 +0200

libtext-sass-perl (0.97-1) unstable; urgency=low

  [ upstream ]
  * New upstream release.
    + comma-separated selectors.
    + scss to css for nested includes.

  [ Jonas Smedegaard ]
  * Bump dephelper compatibility level to 8.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 10 Dec 2012 02:11:32 +0100

libtext-sass-perl (0.96-1) unstable; urgency=low

  * New upstream release.

  * Update package relations:
    + Tighten build-dependency on libmodule-build-perl: Upstream change.
    + Relax to (build-)depend unversioned on cdbs: Needed version
      satisfied in stable, and oldstable no longer supported.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 Sep 2012 19:42:22 +0200

libtext-sass-perl (0.95-1) unstable; urgency=low

  * New upstream release.

  * Bump standards-version to 3.9.3.
  * Use anonscm.debian.org for Vcs-Browser field.
  * Update copyright file: Bump format to 1.0.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 05 Apr 2012 14:37:19 +0200

libtext-sass-perl (0.93-1) unstable; urgency=low

  * New upstream release.

  [ gregor herrmann ]
  * Remove debian/source/local-options: abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.

  [ Florian Schlichting ]
  * Bump Standards-Version to 3.9.2.

  [ Jonas Smedegaard ]
  * Update copyright file:
    + Bump format to latest draft of DEP-5.
    + Quote license names in comments.
  * Update package relations:
    + Relax build-depend unversioned on debhelper and devscripts (needed
      versions satisfied even in oldstable).
  * Bump debhelper compat level to 7.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 31 Jan 2012 22:59:57 +0100

libtext-sass-perl (0.9.2-1) unstable; urgency=low

  * New upstream release.
  * Fix declare recommends.
  * Update copyright file:
    + Rewrite using Subversion rev.173 of draft DEP5 format.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 12 Mar 2011 16:08:12 +0100

libtext-sass-perl (0.9.1-1) unstable; urgency=low

  * New upstream release:
    + Support added for SCSS format!
  * (Build-)depend on libconvert-color-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 05 Dec 2010 11:12:33 +0100

libtext-sass-perl (0.8.1-1) unstable; urgency=low

  * New upstream release.
  * Mangle upstream version string to suppress 'v' affix.
  * Ease building with git-buildpackage: Git-ignore quilt .pc dir, and
    add source local-options.
  * Shorten control file Vcs-* fields.
  * Bump Policy compliance to standards-version 3.9.1.
  * Build-depend on libtry-tiny-perl.
  * Strip suffix from new CLI tool sass2css.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 30 Oct 2010 01:05:49 +0200

libtext-sass-perl (0.5-1) unstable; urgency=low

  * Initial release. Closes: bug#581638.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 14 May 2010 17:13:15 +0200
